package com.epita.mti.geovelib;

import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samybenyoub on 15/05/2017.
 */

public class RepoData {

    private final ArrayList<Record> records;

    public ArrayList<Record> getRecords() {
        return records;
    }

    public RepoData(ArrayList<Record> records){
        this.records = records;
    }

}
