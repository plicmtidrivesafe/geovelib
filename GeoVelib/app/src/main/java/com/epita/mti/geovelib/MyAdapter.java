package com.epita.mti.geovelib;

import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samybenyoub on 12/05/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> implements Filterable {
    private ArrayList<Record> mDataset;
    private ArrayList<Record> mFilteredList;
    private Context pActivity;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mDataset;
                    System.out.println("empty !!!!");
                } else {

                    ArrayList<Record> filteredList = new ArrayList<>();

                    for (Record androidVersion : mDataset) {

                        if (androidVersion.getFields().getAddress().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                for (Record r : mFilteredList) {
                    System.out.println("city = " + r.getFields().getAddress());
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Record>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mTextView;
        private ImageView mImage;
        private Record mItem;
        private ArrayList<Record> repoList;
        private Context pActivity;
        public static final String REPO_EXTRA = "REPO";
        public static final String REPO_LIST_EXTRA = "REPO_LIST";
        public ViewHolder(View v, Context ctx, ArrayList<Record> data) {
            super(v);
            v.setOnClickListener(this);
            mTextView = (TextView) v.findViewById(R.id.list_text_item);
            mImage = (ImageView) v.findViewById(R.id.list_image_item);
            pActivity = ctx;
            repoList = data;
        }

        public void setItem(Record item) {
            mItem = item;
            mTextView.setText(item.getFields().getName());
            if (item.getFields().getStatus().equals("CLOSED")){
                mImage.setImageResource(R.drawable.ic_closed);
            }
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(pActivity, InformationsActivity.class);
            intent.putExtra(REPO_EXTRA, mItem);
            intent.putExtra(REPO_LIST_EXTRA, (ArrayList<? extends Parcelable>) repoList);
            pActivity.startActivity(intent);
        }
    }
    public MyAdapter(ArrayList<Record> myDataset, Context ctx) {
        mDataset = myDataset;
        pActivity = ctx;
        mFilteredList = myDataset;
    }
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line, parent, false);
        return new ViewHolder(v, pActivity, mDataset);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(mFilteredList.get(position));
    }
    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }
}