package com.epita.mti.geovelib;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by samybenyoub on 15/05/2017.
 */

public class Record implements Parcelable {

    private final Fields fields;

    public Fields getFields() {
        return fields;
    }

    public Record(Fields fields){
        this.fields = fields;
    }

    public static final Parcelable.Creator<Record> CREATOR = new Parcelable.Creator<Record>()
    {
        @Override
        public Record createFromParcel(Parcel source)
        {
            return new Record(source);
        }
        @Override
        public Record[] newArray(int size)
        {
            return new Record[size];
        }
    };

    private Record(Parcel source) {
        fields = source.readParcelable(Fields.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(fields, flags);
    }

    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Record))return false;
        Record otherMyClass = (Record) other;

        if (otherMyClass.getFields().getAddress().equals(this.getFields().getAddress())){
            return true;
        } else {
            return false;
        }
    }

}
