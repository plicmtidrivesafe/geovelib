package com.epita.mti.geovelib;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by julien on 10/06/2017.
 */
public class CreditFragment extends AppCompatActivity {

    private TextView linkedinSamy;
    private TextView linkedinJulien;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_design_profile_screen_xml_ui_design);
        linkedinSamy = (TextView)findViewById(R.id.LinkedInSamy);
        linkedinJulien = (TextView)findViewById(R.id.LinkedInJulien);

        linkedinSamy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.linkedin_samy_link)));
                final PackageManager packageManager = CreditFragment.this.getPackageManager();
                final List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                if (list.isEmpty()) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.linkedin_samy_link)));
                }
                startActivity(intent);
            }
        });

        linkedinJulien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.linkedin_julien_link)));
                final PackageManager packageManager = CreditFragment.this.getPackageManager();
                final List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                if (list.isEmpty()) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.linkedin_julien_link)));
                }
                startActivity(intent);
            }
        });
    }
}
