package com.epita.mti.geovelib;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by samybenyoub on 13/05/2017.
 */

public class SectionPageAdapter extends FragmentPagerAdapter {
    private ArrayList<Record> repoArrayList;
    private Toast toast;
    private boolean init;
    private Context context;
    public SectionPageAdapter(FragmentManager fm, ArrayList<Record> mDatasets, Context context) {
        super(fm);
        init = true;
        this.context = context;
        repoArrayList = mDatasets;
    }
    @Override
    public Fragment getItem(int position) {
        if (init)
        {
            init = false;
            if (!SaveChoice.save) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.item_message_info_title).
                        setMessage(R.string.item_message_info_message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SaveChoice.save = true;
                            }
                        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.create().show();
            }

        }
        return PlaceholderFragment.newInstance(repoArrayList.get(position));
    }

    @Override
    public int getCount() {
        return repoArrayList.size();
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return repoArrayList.get(position).getFields().getAddress();
    }
}
