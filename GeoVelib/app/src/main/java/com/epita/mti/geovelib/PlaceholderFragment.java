package com.epita.mti.geovelib;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by samybenyoub on 13/05/2017.
 */

public class PlaceholderFragment extends Fragment {
    private static final String ARG_DATA = "data";
    private Intent intent;
    private GoogleMap GMap;
    private LatLng yaounde;
    private View rootView;

    public static PlaceholderFragment newInstance(Record data) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }
    public PlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ///ashView rootView = inflater.inflate(R.layout.infos, container, false);
        if(rootView == null){
            rootView = inflater.inflate(R.layout.infos, container, false);
        }
        TextView textView = (TextView) rootView.findViewById(R.id.infos_test);
        TextView BikeStands = (TextView) rootView.findViewById(R.id.bike_stands);
        TextView AvailableBikeStands = (TextView) rootView.findViewById(R.id.available_bike_stands);
        TextView StationAdress = (TextView) rootView.findViewById(R.id.adress_station);
        TextView LastUpdate = (TextView) rootView.findViewById(R.id.last_update);
        ImageView status = (ImageView) rootView.findViewById(R.id.status_infos);

        GMap= ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();

        final Record r = getArguments().getParcelable(ARG_DATA);

        yaounde = new LatLng(Double.parseDouble(r.getFields().getPosition().get((0)).toString()), Double.parseDouble(r.getFields().getPosition().get((1)).toString()));
        if(GMap != null){
            GMap.setTrafficEnabled(true);
            GMap.setMyLocationEnabled(true);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.title(r.getFields().getName());
            markerOptions.visible(true);
            markerOptions.position(yaounde);
            if (r.getFields().getStatus().equals("CLOSED")) {
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                markerOptions.snippet(r.getFields().getLast_update());
            } else if (r.getFields().getStatus().equals("OPEN")) {
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                System.out.println("---------------- info bike available = " + r.getFields().getAvailable_bikes_stands());
                if (Integer.parseInt(r.getFields().getAvailable_bikes_stands()) > 1) {
                    markerOptions.snippet(r.getFields().getAvailable_bikes_stands() + R.string.google_maps_snipet_plural);
                } else if (Integer.parseInt(r.getFields().getAvailable_bikes_stands()) == 1) {
                    markerOptions.snippet(r.getFields().getAvailable_bikes_stands() + R.string.google_maps_snipet_simple);
                }
                markerOptions.snippet(r.getFields().getAvailable_bikes_stands() + " vélos disponibles");
            }

            GMap.addMarker(markerOptions);
            GMap.moveCamera(CameraUpdateFactory.newLatLngZoom(yaounde, 16));
            GMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }



        FloatingActionButton actionFloatButton = (FloatingActionButton) rootView.findViewById(R.id.floattingButton);
        actionFloatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder str = new StringBuilder("geo:");
                str.append(r.getFields().getPosition().get(0));
                str.append(",");
                str.append(r.getFields().getPosition().get(1));
                str.append("?q=");
                str.append(r.getFields().getPosition().get(0));
                str.append(",");
                str.append(r.getFields().getPosition().get(1));
                str.append("(");
                str.append(r.getFields().getAddress());
                str.append(")");
                Uri uri = Uri.parse(str.toString());
                intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                try
                {AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.google_maps_title).
                            setMessage(R.string.google_maps_message)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    builder.create().show();
                }
                catch(ActivityNotFoundException ex)
                {
                    try
                    {
                        Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(unrestrictedIntent);
                    }
                    catch(ActivityNotFoundException innerEx)
                    {
                        Toast.makeText(getActivity(), "Please install a maps application", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });



        if (r.getFields().getStatus().equals("CLOSED")){
            status.setImageResource(R.drawable.ic_closed);
        }
        textView.setText(r.getFields().getName());
        BikeStands.setText(r.getFields().getBikes_stands());
        AvailableBikeStands.setText(r.getFields().getAvailable_bikes_stands());
        int available = Integer.parseInt(r.getFields().getAvailable_bikes_stands());
        int max = Integer.parseInt(r.getFields().getBikes_stands());
        if (available >= 0){
            AvailableBikeStands.setTextColor(Color.RED);
        }  if (available > max * 0.2) {
            AvailableBikeStands.setTextColor(Color.YELLOW);
        }  if (available > max * 0.5) {
            AvailableBikeStands.setTextColor(Color.GREEN);
        }
        LastUpdate.setText(r.getFields().getLast_update());
        StationAdress.setText(r.getFields().getAddress());

        //textView.setText("Section : " + r.getFields().getAddress());
        StationAdress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder str = new StringBuilder("geo:");
                str.append(r.getFields().getPosition().get(0));
                str.append(",");
                str.append(r.getFields().getPosition().get(1));
                str.append("?q=");
                str.append(r.getFields().getPosition().get(0));
                str.append(",");
                str.append(r.getFields().getPosition().get(1));
                str.append("(");
                str.append(r.getFields().getAddress());
                str.append(")");
                Uri uri = Uri.parse(str.toString());
                intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                try
                {AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.google_maps_title).
                            setMessage(R.string.google_maps_message)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    builder.create().show();
                }
                catch(ActivityNotFoundException ex)
                {
                    try
                    {
                        Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(unrestrictedIntent);
                    }
                    catch(ActivityNotFoundException innerEx)
                    {
                        Toast.makeText(getActivity(), "Please install a maps application", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        return rootView;
    }
}