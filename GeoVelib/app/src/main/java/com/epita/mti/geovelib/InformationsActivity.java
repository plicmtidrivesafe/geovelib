package com.epita.mti.geovelib;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.epita.mti.geovelib.MyAdapter.ViewHolder.REPO_EXTRA;
import static com.epita.mti.geovelib.MyAdapter.ViewHolder.REPO_LIST_EXTRA;

/**
 * Created by samybenyoub on 12/05/2017.
 */

public class InformationsActivity extends AppCompatActivity {
    private Record mRepo;
    private ArrayList<Record> mDatasets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.informations_layout);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(myToolbar);

        final Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(REPO_EXTRA)) {
                mRepo = intent.getParcelableExtra(REPO_EXTRA);
            }
            if (intent.hasExtra(REPO_LIST_EXTRA)) {
                mDatasets = intent.getParcelableArrayListExtra(REPO_LIST_EXTRA);
            }
        }

        TextView textView = (TextView) findViewById(R.id.infos_text);
        textView.setText(mRepo.getFields().getAddress());

       Collections.swap(mDatasets, 0, mDatasets.indexOf(mRepo));
        SectionPageAdapter mSectionsPagerAdapter = new
                SectionPageAdapter(getSupportFragmentManager(), mDatasets, InformationsActivity.this);
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, mRepo.getFields().getAddress());
                startActivity(intent);
                return true;
            case R.id.action_credit:
                Intent i = new Intent(this, CreditFragment.class);
                this.startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
