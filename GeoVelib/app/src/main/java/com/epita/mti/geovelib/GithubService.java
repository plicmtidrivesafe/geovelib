package com.epita.mti.geovelib;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by samybenyoub on 12/05/2017.
 */

public interface GithubService {
    //public static final String ENDPOINT = "http://www.tutos-android.com";
    //@GET("/MTI/2018/TP3.json")
    //Call<List<Repo>> searchRepos();

    public static final String ENDPOINT = "https://opendata.paris.fr";
    @GET("/api/records/1.0/search/?dataset=stations-velib-disponibilites-en-temps-reel&rows=100&facet=banking&facet=bonus&facet=status&facet=contract_name")
    Call<RepoData> searchRepos();
}
