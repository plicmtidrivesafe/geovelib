package com.epita.mti.geovelib;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by samybenyoub on 12/05/2017.
 */

public class Repo implements Parcelable{

    private final String uri;
    private final String title;
    private final String auteur;
    private final String type;

    public Repo(String uri, String title, String auteur, String type) {
        this.title = title;
        this.auteur = auteur;
        this.type = type;
        this.uri = uri;
    }

    public String display(){
        return title.toString();
    }

    public String getTitle() {
        return title;
    }

    public static final Parcelable.Creator<Repo> CREATOR = new Parcelable.Creator<Repo>()
    {
        @Override
        public Repo createFromParcel(Parcel source)
        {
            return new Repo(source);
        }
        @Override
        public Repo[] newArray(int size)
        {
            return new Repo[size];
        }
    };

    private Repo(Parcel source) {
        uri = source.readString();
        title = source.readString();
        auteur = source.readString();
        type = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uri);
        dest.writeString(title);
        dest.writeString(auteur);
        dest.writeString(type);
    }
}
