package com.epita.mti.geovelib;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by samybenyoub on 15/05/2017.
 */

public class Fields implements Parcelable{

    private final String status;
    private final String name;
    private final String bike_stands;
    private final String number;
    private final String last_update;
    private final String available_bike_stands;
    private final String address;
    private final ArrayList<String> position;


    public String getAddress() {
        return address;
    }

    public ArrayList getPosition(){
        return  position;
    }

    public String getStatus() {
        return status;
    }

    public String getName(){
        return name;
    }

    public String getBikes_stands(){
        return bike_stands;
    }

    public String getLast_update(){
        return last_update;
    }

    public String getAvailable_bikes_stands(){
        return available_bike_stands;
    }



    public Fields(String mStatus, String mNumber, String mLastupdate,
                  String mAvailablebike, String mAddress, ArrayList<String> mCoordinates, String mName,
                  String mBikestands) {
        status = mStatus;
        number = mNumber;
        last_update = mLastupdate;
        available_bike_stands = mAvailablebike;
        address = mAddress;
        position = mCoordinates;
        name = mName;
        bike_stands = mBikestands;
    }

    public static final Parcelable.Creator<Fields> CREATOR = new Parcelable.Creator<Fields>()
    {
        @Override
        public Fields createFromParcel(Parcel source)
        {
            return new Fields(source);
        }
        @Override
        public Fields[] newArray(int size)
        {
            return new Fields[size];
        }
    };

    private Fields(Parcel source) {
        status = source.readString();
        name = source.readString();
        bike_stands = source.readString();
        number = source.readString();
        last_update = source.readString();
        available_bike_stands = source.readString();
        address = source.readString();
        position = source.readArrayList(String.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(name);
        dest.writeString(bike_stands);
        dest.writeString(number);
        dest.writeString(last_update);
        dest.writeString(available_bike_stands);
        dest.writeString(address);
        dest.writeList(position);
    }
}
